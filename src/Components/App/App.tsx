import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import AddContact from "../AddContact/AddContact";
import DeleteContact from "../DeleteContact/DeleteContact";
import EditContact from "../EditContact/EditContact";
import Header from "../Header/Header";
import ListContacts from "../ListContacts/ListContacts";
import ViewContact, { IContact } from "../ViewContact/ViewContact";

interface IAppState {
    contacts: IContact[];
    selectedContactId: string | null;
}

class App extends Component<{}, IAppState> {
    render() {
        return (
            <main>
                <Header />

                <main>
                    <Switch>
                        <Route path="/contact/new" component={AddContact} />
                        <Route path="/contact/:id/delete" component={DeleteContact} />
                        <Route path="/contact/:id/edit" component={EditContact} />
                        <Route path="/contact/:id" component={ViewContact} />
                        <Route path="/contacts" component={ListContacts} />
                        <Redirect to="/contacts" />
                    </Switch>
                </main>
            </main>
        );
    }
}

export default App;

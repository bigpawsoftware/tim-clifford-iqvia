import React from "react";
import { shallow } from "enzyme";

import DeleteContact from "./DeleteContact";

it("renders without crashing", () => {
    const wrapper = shallow(<DeleteContact history={[]} match={{ params: { id: 'whatever' }, isExact: true, path: "", url: ""}} />);

    expect(wrapper).toMatchSnapshot();
});

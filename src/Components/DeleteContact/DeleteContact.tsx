import React, { Component } from "react";
import { match } from "react-router-dom";

import { Mutation, Query } from "react-apollo";

import { Button, Paper, Typography } from "@material-ui/core";
import { Cancel, Delete } from "@material-ui/icons";

import { IContact } from "../ViewContact/ViewContact";
import { ALL_CONTACTS_QUERY, CONTACT_QUERY, DELETE_CONTACT_MUTATION } from "../../queries";

interface DeleteContactProps {
    history: Array<any>,
    match: match<{ id: string }>,
}
class DeleteContact extends Component<DeleteContactProps> {
    goToContactsList = (): void => {
        this.props.history.push("/contacts");
    };

    onCancel = () => {
        this.props.history.push(`/contact/${this.props.match.params.id}`);
    };

    updateCache = (cache: any, { data }: any) => {
        if (!data) return;

        const { contacts } = cache.readQuery({ query: ALL_CONTACTS_QUERY }) || { contacts: [] };

        cache.writeQuery({
            query: ALL_CONTACTS_QUERY,
            data: { contacts: contacts.filter((contact: IContact) => contact.id !== this.props.match.params.id) },
        });
    };

    render() {
        const { id } = this.props.match.params;

        return (
            <Query query={CONTACT_QUERY} variables={{ id }}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Something went wrong!</div>;

                    const { name } = data.contact;

                    return (
                        <Paper className="paper">
                            <Typography variant="body2" component="p">
                                Do you really want to delete the contact for {name}?
                            </Typography>

                            <Button
                                className="actionButton"
                                color="primary"
                                onClick={this.onCancel}
                                variant="contained"
                            >
                                Cancel
                                <Cancel aria-label="Cancel" className="actionButtonIcon" />
                            </Button>

                            <Mutation mutation={DELETE_CONTACT_MUTATION} update={this.updateCache} onCompleted={this.goToContactsList}>
                                {(deleteContact, { error, loading }) => {
                                    if (loading) return <div>Loading...</div>;
                                    if (error) return <div>Something went wrong!</div>;

                                    return (
                                      <Button
                                          className="actionButton"
                                          color="secondary"
                                          onClick={() => deleteContact({ variables: { id } })}
                                          variant="contained"
                                      >
                                        Delete
                                        <Delete aria-label="Delete" className="actionButtonIcon" />
                                      </Button>
                                    );
                                }}
                            </Mutation>
                        </Paper>
                    );
                }}
            </Query>
        );
    }
}

export default DeleteContact;

import React from "react";
import { shallow } from "enzyme";

import ViewContact from "./ViewContact";

it("renders without crashing", () => {
    const wrapper = shallow(<ViewContact match={{ params: { id: 'whatever' }, isExact: true, path: "", url: ""}} />);

    expect(wrapper).toMatchSnapshot();
});

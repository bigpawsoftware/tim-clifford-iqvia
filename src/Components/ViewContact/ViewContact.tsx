import React, { Component } from "react";
import { Link, match } from "react-router-dom";

import { Query } from "react-apollo";

import { CONTACT_QUERY } from "../../queries";

import { Button, Card, CardContent, Typography } from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons";

export interface IContact {
    email: string,
    id: string,
    name: string,
}

interface IContactProps {
    match: match<{ id: string }>,
}

class ViewContact extends Component<IContactProps> {
    render() {
        return (
            <Query query={CONTACT_QUERY} variables={{ id: this.props.match.params.id }}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Something went wrong!</div>;

                    const { email, id, name } = data.contact;

                    return (
                        <Card className="paper">
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    {name}
                                </Typography>

                                <Typography variant="body2" component="p">
                                    {email}
                                </Typography>

                                <Link to={`/contact/${id}/delete`}>
                                    <Button variant="contained" color="secondary" className="actionButton">
                                        Delete
                                        <Delete aria-label="Delete" className="actionButtonIcon" />
                                    </Button>
                                </Link>
                                <Link to={`/contact/${id}/edit`}>
                                    <Button variant="contained" color="primary" className="actionButton">
                                        Edit
                                        <Edit aria-label="Edit" className="actionButtonIcon" />
                                    </Button>
                                </Link>
                            </CardContent>
                        </Card>
                    );
                }}
            </Query>
        );
    }
}

export default ViewContact;

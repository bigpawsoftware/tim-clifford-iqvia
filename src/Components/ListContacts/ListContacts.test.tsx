import React from "react";
import { shallow } from "enzyme";

import ListContacts from "./ListContacts";

it("renders without crashing", () => {
    const wrapper = shallow(<ListContacts />);

    expect(wrapper).toMatchSnapshot();
});

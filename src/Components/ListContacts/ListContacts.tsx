import React, { Component } from "react";
import { Link } from "react-router-dom";

import { Query } from "react-apollo";

import { ALL_CONTACTS_QUERY } from "../../queries";

import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";

import { IContact } from "../ViewContact/ViewContact";

class ListContacts extends Component<{}, {}> {
    render() {
        return (
            <Query query={ALL_CONTACTS_QUERY}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Something went wrong!</div>;

                    return (
                        <List component="nav" aria-label="Contacts">
                            {data.contacts.map((contact: IContact) => (
                                <Link to={`/contact/${contact.id}`} key={contact.id}>
                                    <ListItem button>
                                        <ListItemIcon>
                                            <AccountCircle/>
                                        </ListItemIcon>
                                        <ListItemText primary={contact.name}/>
                                    </ListItem>
                                </Link>
                            ))}
                        </List>
                    );
                }}
            </Query>
        );
    }
}

export default ListContacts;

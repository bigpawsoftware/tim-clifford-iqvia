import React, { Component, ChangeEvent } from "react";

import nanoid from "nanoid";
import { Mutation } from "react-apollo";

import { IContact } from "../ViewContact/ViewContact";
import { ADD_CONTACT_MUTATION, ALL_CONTACTS_QUERY } from "../../queries";

import { Button, Paper, TextField } from "@material-ui/core";
import { Cancel, Check } from "@material-ui/icons";

interface IAddContactProps {
    history: Array<any>,
}

interface IAddContactState {
    contact: IContact,
}

class AddContact extends Component<IAddContactProps, IAddContactState> {
    state = {
        contact: { id: nanoid(), email: "", name: "" }, // The backend ignores the id anyway, but it's in the schema
    };

    goToContactsList = (): void => {
        this.props.history.push("/contacts");
    };

    onChangeEmail = (event: ChangeEvent<HTMLInputElement>) => {
        const email = event.target.value;

        this.setState(state => ({
            contact: {
                ...state.contact,
                email,
            },
        }));
    };

    onChangeName = (event: ChangeEvent<HTMLInputElement>) => {
        const name = event.target.value;

        this.setState(state => ({
            contact: {
                ...state.contact,
                name,
            },
        }));
    };

    updateCache = (cache: any, { data }: any) => {
        if (!data) return;

        const { contacts } = cache.readQuery({ query: ALL_CONTACTS_QUERY }) || { contacts: [] };

        cache.writeQuery({
            query: ALL_CONTACTS_QUERY,
            data: { contacts: contacts.concat({ ...data.addContact })},
        });
    };

    render() {
        return (
            <Mutation mutation={ADD_CONTACT_MUTATION} update={this.updateCache} onCompleted={this.goToContactsList}>
                {(addContact, { error, loading }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Something went wrong!</div>;

                    return (
                        <Paper className="paper">
                            <form>
                                <TextField
                                    error={this.state.contact.name.trim() === ""}
                                    fullWidth
                                    id="name"
                                    label="Name"
                                    margin="normal"
                                    onChange={this.onChangeName}
                                    required
                                    value={this.state.contact.name}
                                    variant="outlined"
                                />

                                <TextField
                                    error={this.state.contact.email.trim() === ""}
                                    fullWidth
                                    id="email"
                                    label="Email address"
                                    margin="normal"
                                    onChange={this.onChangeEmail}
                                    required
                                    type="email"
                                    value={this.state.contact.email}
                                    variant="outlined"
                                />

                                <Button
                                    className="actionButton"
                                    color="secondary"
                                    onClick={this.goToContactsList}
                                    variant="contained"
                                >
                                    Cancel
                                    <Cancel aria-label="Cancel" className="actionButtonIcon" />
                                </Button>

                                <Button
                                    color="primary"
                                    disabled={this.state.contact.name.trim() === "" || this.state.contact.email.trim() === ""}
                                    onClick={() => {
                                        addContact({ variables: { contact: this.state.contact } });
                                    }}
                                    variant="contained"
                                >
                                    Submit
                                    <Check aria-label="Submit" className="actionButtonIcon" />
                                </Button>
                            </form>
                        </Paper>
                    );
                }}
            </Mutation>
        );
    }
}

export default AddContact;

import React from "react";
import { shallow } from "enzyme";

import AddContact from "./AddContact";

it("renders without crashing", () => {
    const wrapper = shallow(<AddContact history={[]} />);

    expect(wrapper).toMatchSnapshot();
});

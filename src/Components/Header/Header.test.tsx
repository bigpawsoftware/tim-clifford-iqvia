import React from "react";
import { shallow } from "enzyme";

import Header from "./Header";

it("renders without crashing", () => {
    const wrapper = shallow(<Header history={[]} location={{ pathname: 'whatever' }} />);

    expect(wrapper).toMatchSnapshot();
});

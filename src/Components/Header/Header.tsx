import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { AppBar, Tab, Tabs, Typography } from "@material-ui/core";

interface IHeaderProps {
    history: Array<any>,
    location: any,
}

class Header extends Component<IHeaderProps> {
    render() {
        const value = this.props.location.pathname.endsWith("/new") ? 1 : 0;

        return (
            <header>
                <AppBar position="static">
                    <Tabs variant="fullWidth" value={value}>
                        <Tab label="List contacts" onClick={() => { this.props.history.push("/contacts"); }} />
                        <Tab label="Add contact" onClick={() => { this.props.history.push("/contact/new"); }} />
                    </Tabs>
                </AppBar>

                <Typography variant="h1" component="h1">
                    Contacts
                </Typography>
            </header>
        );
    }
}

export default withRouter<any>(Header);

import React, { Component, createRef } from "react";
import { match } from "react-router";

import { Mutation, Query } from "react-apollo";

import { IContact } from "../ViewContact/ViewContact";
import { CONTACT_QUERY, UPDATE_CONTACT_MUTATION } from "../../queries";

import { Button, TextField, Paper } from "@material-ui/core";
import { Cancel, Check } from "@material-ui/icons";

interface IEditContactProps {
    history: Array<any>,
    match: match<{ id: string }>,
}

interface IEditContactState {
    contact: IContact,
}

class EditContact extends Component<IEditContactProps, IEditContactState> {
    nameRef: any;
    emailRef: any;

    constructor(props: IEditContactProps) {
        super(props);

        this.nameRef = createRef();
        this.emailRef = createRef();
    }

    goToContactsList = (): void => {
        this.props.history.push("/contacts");
    };

    onCancel = () => {
        this.props.history.push(`/contact/${this.props.match.params.id}`);
    };

    render() {
        const { id } = this.props.match.params;

        return (
            <Query query={CONTACT_QUERY} variables={{ id }}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Loading...</div>;
                    if (error) return <div>Something went wrong!</div>;

                    const { email, name } = data.contact;

                    return (
                        <Paper className="paper">
                            <Mutation mutation={UPDATE_CONTACT_MUTATION} onCompleted={this.goToContactsList}>
                                {(updateContact, { error, loading }) => {
                                    if (loading) return <div>Loading...</div>;
                                    if (error) return <div>Something went wrong!</div>;

                                    return (
                                        <form>
                                            <TextField
                                                defaultValue={name}
                                                fullWidth
                                                id="name"
                                                inputRef={this.nameRef}
                                                label="Name"
                                                margin="normal"
                                                required
                                                variant="outlined"
                                            />

                                            <TextField
                                                defaultValue={email}
                                                fullWidth
                                                id="email"
                                                inputRef={this.emailRef}
                                                label="Email address"
                                                margin="normal"
                                                required
                                                type="email"
                                                variant="outlined"
                                            />

                                            <Button
                                                className="actionButton"
                                                color="secondary"
                                                onClick={this.onCancel}
                                                variant="contained"
                                            >
                                                Cancel
                                                <Cancel aria-label="Cancel" className="actionButtonIcon" />
                                            </Button>

                                            <Button
                                                color="primary"
                                                onClick={() => {
                                                    if (this.nameRef.current.value.trim() === "" || this.emailRef.current.value.trim() === "") return;

                                                    updateContact({
                                                        variables:
                                                            {
                                                                contact: {
                                                                    id,
                                                                    name: this.nameRef.current.value,
                                                                    email: this.emailRef.current.value,
                                                                }
                                                            }
                                                    })
                                                }}
                                                variant="contained"
                                            >
                                                Submit
                                                <Check aria-label="Submit" className="actionButtonIcon" />
                                            </Button>
                                        </form>
                                    );
                                }}
                            </Mutation>
                        </Paper>
                    );
                }}
            </Query>
        );
    }
}

export default EditContact;

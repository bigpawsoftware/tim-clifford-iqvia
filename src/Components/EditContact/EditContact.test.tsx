import React from "react";
import { shallow } from "enzyme";

import EditContact from "./EditContact";

it("renders without crashing", () => {
    const wrapper = shallow(<EditContact history={[]} match={{ params: { id: 'whatever' }, isExact: true, path: "", url: ""}} />);

    expect(wrapper).toMatchSnapshot();
});

import gql from "graphql-tag";

export const ADD_CONTACT_MUTATION = gql`
  mutation AddContact($contact: InputContact!) {
    addContact(contact: $contact) {
      id
      name
      email
    }
  }`;

export const ALL_CONTACTS_QUERY = gql`
  {
    contacts {
      id
      name
      email
    }
  }`;

export const CONTACT_QUERY = gql`
  query Contact($id: ID!) {
    contact(id: $id) {
      id
      name
      email
    }
  }`;

export const DELETE_CONTACT_MUTATION = gql`
  mutation DeleteContact($id: ID!) {
    deleteContact(id: $id)
  }`;

export const UPDATE_CONTACT_MUTATION = gql`
  mutation UpdateContact($contact: InputContact!) {
    updateContact(contact: $contact) {
      id
      name
      email
    }
  }`;
